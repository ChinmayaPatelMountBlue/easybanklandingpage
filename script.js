const dropdown = document.querySelector('.btn-dropdown');
const navbar = document.querySelector('header .navbar');

dropdown.addEventListener('click', () => {

    if(navbar.classList.contains('active')){

        navbar.classList.remove('active');

        dropdown.innerHTML = `<img src="./images/icon-hamburger.svg"/>`

    }

    else{

        navbar.classList.add('active');

        dropdown.innerHTML = `<img src="./images/icon-close.svg"/>`;

    }

})